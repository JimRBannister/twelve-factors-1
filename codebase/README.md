## Twelve Factors - codebase 

Create a new project called mock-graphql in GitLab against your own GitLab account
[GitLab](https://gitlab.com/) or Github, whichever you prefer. 

Use the "Clone" button to take a copy of the _**repository url**_. (copy from the https option - copy URL)

From your own machine's command window (Note: the repository_url is the value copied from the "Clone")

```bash
git clone repository_url
```


change directory to the new sub-directory just created by _git clone_

For example

```bash
cd mock-graphql
```

create a new branch for our initial changes
```bash
git checkout -b dev
```
This dev branch will contain our completed features and fixes. This dev branch is the working tree for our source repository.

All new development is undertaken within a feature branch. Feature branches are always branched off of the dev branch and prefixed with a feature slash.

Push the new branch upstream so it also held with the GitLab repository

```bash
git push -u origin dev
```

the command will return
Branch 'dev' set up to track remote branch 'dev' from 'origin'.

```bash
git checkout -b feature/setup dev
```


We will now create 4 new files in this directory so that we end up with a  working mock. 

Create a new package file via the npm utility. Use the defaults when prompted.

```bash
npm init 
```


Install the apollo server, and two graphql tools packages. 
```bash
npm install @apollo/server @graphql-tools/mock @graphql-tools/schema
```

edit the package.json file and add the following to the scripts section 

```
    "start" : "node index.js",
```
and

Adding "type": "module" to the package.json enables ES 6 modules and so allows import statements to be used. 
Please add the following line to the line before the scripts section:

```
  "type": "module",
```

Part of the package.json file should now look like this:-

```
  "type": "module",
  "scripts": {
    "start" : "node index.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
```

create a new .gitignore file (this file is used by git so that any directories or files you don't wish to hold in the repository can be ignored) with the contents as follows:

```
	node_modules/
```


create an index.js with the following contents
```
import {ApolloServer} from '@apollo/server';
import {startStandaloneServer} from '@apollo/server/standalone';

import { addMocksToSchema } from '@graphql-tools/mock';
import { makeExecutableSchema } from '@graphql-tools/schema'

const typeDefs = `
     #graphql
	 type Temperature {
	    location: String
	    temperature:Float
	    recordedat: Int
	  }
	  type Query {
	    allTemperatures: [Temperature]
	    recentTemperatures: [Temperature]
	  }
	  type Mutation {
	    addTemperature(location: String!, temperature: Float!): Boolean
	  }
	`;

const resolvers = {
    Mutation: {
        addTemperature: (parent, args, context) => {
            return true
        }
    }
};

const server = new ApolloServer({
	schema: addMocksToSchema({
		schema: makeExecutableSchema({ typeDefs, resolvers }),
	})
});

const {url} = await startStandaloneServer(server, {
    context: async ({req}) => ({token: req.headers.token}),
    listen: {port: 4000},
});

console.log(`🚀  Server ready at ${url}`);

	
```
Check the correct apostrophe is used in the file. There can be issues with character conversions. 

Now let's fire up the server!! 

```bash
npm start
```

After a successful start you will see the message starting with
```
🚀  Server ready at http://localhost:4000/

```
In a separate shell attempt or go to a web page and use the web address above to "Query your Server"

Query via the Web Sandbox for GraphQL:
```
query ExampleQuery {
   allTemperatures {
     temperature
   }
}
```
Query via curl as an alternative (run the query allTemperatures):
```bash
curl --request POST  -H 'Content-Type: application/json'  --data '{"query":"query  allTemperatures{allTemperatures {temperature}}", "operationName":"allTemperatures"}' --url  http://localhost:4000/
```


Abort the service by using  CTRL & c  together when finished.


Add the files to the feature branch after a successful test

```bash
git add index.js package.json yarn.lock .gitignore
```


Commit your changes
```bash	
git commit -m "setup graphql mock server"
```

Now let's merge our changes from the feature branch into dev

```bash
git checkout dev
git merge feature/setup
git push
```

Ensure our local dev branch is up to date!

```bash
git rebase
```

dev has now been updated with the feature branch.

Now we wish our new feature to be incorporated into the main branch to ensure that our development and production code bases do not diverge too much and so maintain dev/prod partity.

Attempt to checkout the main, merge the changes from dev, and push those changes to the repository.

Check your changes are reflected in the main branch via your GitLab project web page.

Congratulations. You have completed the task.

Further information about GraphQL Mocking can be found at https://www.apollographql.com/docs/apollo-server/testing/mocking/

